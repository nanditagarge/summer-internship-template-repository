# Team Hash It Out - Image Auto Compression Engine

We offer a unique one-stop solution to compressing,decompressing,cropping and resizing of your images while offering the facility of storing all the compressed images securely. All images are stored in individual user accounts which can be decompressed and downloaded.  

## Project Overview

Images take up a lot of storage space in any device, they also increase any website's loading time, hence image optimisation is a nessecity now. It helps in improving page load speed, improves the speed of sharing the images over the net and improves user experience. 

Our project provides the solution for image optimization by the use of the Huffman algorithm that uses lossless compression with best possible quality retention. We have a  User friendly Interface that is comfortable to use and easy to navigate. We also provide options to crop, resize the images and then store it in a database. We also display the compression details for the user to know the difference.
 
## Solution Description
 
_____________

### Architecture Diagram
![Diagram](Documentation/workflow.png)
______________

### Technical Description

* Technologies/versions used :
   * Laravel 8
   * PHP 7.4
   * Python 3
* Setup/Installations required :
   * PIL Module
   * heapq Module
   * Composer
   * Larvel Framework

* Instructions to run the code :
   * Fire any APACHE Server
   * Navigate to the desired location and then append '/public' to get it started
   * Load the SQL database into your sytem and update the host, database, username, password in the .env file

## Team Members
1. Aradhana Das   b118016@iiit-bh.ac.in
    - Resize algorithm
    - Login Page
    - Signup Page
    - Dynamic Cropping

2. Nanditha Garge  b118028@iiit-bh.ac.in
    - Decompression algorithm
    - UI Design
    - S3 storage
    - Main Page

3. Riya Ugemuge   b418039@iiit-bh.ac.in
    - Resize Algorithm
    - Database Creation
    - Signup Page
    - Main Page

4. Soutik Nandy   b418053@iiit-bh.ac.in
    - Compression algorithm
    - Decompression algorithm
    - Dynamic Cropping
    - Code merging using Laravel