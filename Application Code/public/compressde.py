from PIL import Image
import heapq
import time
import sys

'''
Things this program does:
    1. Loads an image and pixel data as an object of the Picture class
    2. Creates a dictionary of [R,G,B: frequency] for each pixel
    3. Creates a heap of dictionary elements
    4. Recursively merges heap elements into a huff tree
    5. Traverses huff tree to generated encoded strings
    6. Refills dictionary with [R,G,B: encoded strings]
    7. Produces text file containing compressed image data
    
'''


class HeapNode:
    def __init__(self, rgb, freq):
        self.rgb = rgb
        self.freq = freq
        self.left = None
        self.right = None

    def __lt__(self, other):
        return self.freq < other.freq

    def __eq__(self, other):
        if other == None:
            return False
        if(not isinstance(other, HeapNode)):
            return False
        return self.freq == other.freq


class Picture:
    def __init__(self, name):
        self.name = name[:-4]
        self.dict = {}
        self.data = []
        self.heap = []
        self.encode = {}
        self.decode = {}
        self.res_image = []
        self.recieve = []
        self.bits=""
        self.img = Image.open(name)
        self.width, self.height = self.img.size
        self.px = self.img.load()

    def load_data(self):
        for row in range(self.width):
            for col in range(self.height):
                self.data.append(self.px[row, col])
        for item in self.data:
            if item not in self.dict:
                self.dict[item] = 1
            else:
                self.dict[item] += 1

    def display(self, img_object):
        img_object.show()

    def get_data(self):
        return self.data

    def get_dict(self):
        return self.dict

    def make_heap(self):
        for key in self.dict:
            if self.dict[key] > 0:
                node = HeapNode(key, self.dict[key])
                heapq.heappush(self.heap, node)

    def merge_nodes(self):
        while(len(self.heap)>1):
            node_one = heapq.heappop(self.heap)
            node_two = heapq.heappop(self.heap)

            merge = HeapNode(None, node_one.freq + node_two.freq)
            merge.left = node_one
            merge.right = node_two

            heapq.heappush(self.heap, merge)

    def heaporder(self, root, buffer):
        if root:
            self.res_image.append([root.rgb, root.freq, buffer])
            buffer += "0"
            self.heaporder(root.left, buffer)
            buffer = buffer[:-1]
            buffer += "1"
            self.heaporder(root.right, buffer)

    def create_compression_keys(self):
        for item in self.res_image:
            if item[0]:
                self.encode[item[0]] = item[2]
                self.decode[item[2]] = item[0]
    def test(self):
        decompressed = Image.new('RGB', (self.width, self.height))
        pixels = decompressed.load()
        index = 0
        for row in range(self.width):
            for col in range(self.height):
                pixels[row, col] = self.encode[self.res_image[index]]
                index += 1
        self.display(decompressed)
    def writeout(self):
        with open(self.name+"_out.huff", 'w') as out:
            for pix in self.data:
                out.write(self.encode[pix]+"\n")
                # self.bits+=self.encode[pix]

    def readin(self):
        with open(self.name+"_out.huff", 'r') as ins:
            self.recieve = ins.read().splitlines()

    def create_new_image(self,name,bpp):
        if bpp==24:
            decompressed = Image.new('RGB', (self.width, self.height))
        else:
            decompressed = Image.new('RGBA', (self.width, self.height))

        pixels = decompressed.load()
        index = 0
        for row in range(self.width):
            for col in range(self.height):
                pixels[row, col] = self.decode[self.recieve[index]]

                index += 1
        # print(pixels[1,1])
        name=name.split('/')[-1]
        # import ufp.image
        # ufp.image.changeColorDepth(decompressed,bpp)
        decompressed.save('decompressed/'+".".join(name.split('.')[:-1])+'.'+name.split('.')[-1])


numba_one = Picture(sys.argv[1])
# print("images/"+sys.argv[1])
numba_one.load_data()
# print(numba_one.data[:1000])
# print(len(numba_one.data[:1000]))
numba_one.make_heap()
numba_one.merge_nodes()
numba_one.heaporder(numba_one.heap[0], "")
numba_one.create_compression_keys()
numba_one.writeout()
numba_one.readin()
# print(numba_one.decode[numba_one.recieve[3]])
s=0
for i in numba_one.recieve:
    s+=len(i)
n_si=s
mode_to_bpp = {'1':1, 'L':8, 'P':8, 'RGB':24, 'RGBA':32, 'CMYK':32, 'YCbCr':24, 'I':32, 'F':32}
bpp = mode_to_bpp[numba_one.img.mode]
o_si=len(numba_one.data)*bpp
print(o_si)
print(s)
print(((o_si-n_si)/o_si)*100)
numba_one.create_new_image(sys.argv[1],bpp)
