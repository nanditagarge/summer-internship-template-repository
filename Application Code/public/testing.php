<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge"> 
		<meta name="viewport" content="width=device-width, initial-scale=1"> 
		<title>Image Resizing</title>
		<meta name="description" content="Learn how to resize images using JavaScript and the HTML5 Canvas element using controls, commonly seen in photo editing applications." />
		<meta name="keywords" content="canvas, javascript, HTML5, resizing, images" />
		<meta name="author" content="Codrops" />
		<link rel="shortcut icon" href="../favicon.ico">
		<!-- <link rel="stylesheet" type="text/css" href="css/normalize.css" />
		<link rel="stylesheet" type="text/css" href="css/demo.css" />
		<link rel="stylesheet" type="text/css" href="css/component.css" /> -->
	</head>
	<body>
		<div class="container">
			<div class="content">
				<header class="codrops-header">
					<h1>Image Resizing</h1>
				</header>
				<div class="component">
					<div class="overlay">
						<div class="overlay-inner">
						</div>
					</div>
					<!-- This image must be on the same domain as the demo or it will not work on a local file system -->
					<!-- http://en.wikipedia.org/wiki/Cross-origin_resource_sharing -->
					<img  src="imgages/keepthishandy.jpeg" alt="image for resizing">
				</div>
				<div class="a-tip">
					<p><strong>Tip:</strong> hold <span>SHIFT</span> while resizing to keep the original aspect ratio of the image.</p>
				</div>
			</div>	
		</div> 
	</body>
	<script type="text/javascript">
var imageData = Array();
function findAllImages()
{
  var imgs = document.getElementsByTagName('img');
  console.log('1');
  for (i=0; i<imgs.length; i++)
  {

    // We will populate this as the user interacts with the image, if they
    // do at all.
    imageData[imgs[i]] = {
		zindex: imgs[i].style.zIndex,
		width: imgs[i].style.width,
		height: imgs[i].style.height,
		position: imgs[i].style.position,
		resized: 0,
		resizable: true
	};

    makeImageZoomable(imgs[i]);
  }

}

/*
 * Calculate the drag size for the event. This is taken directly from
 * honestbleeps's Reddit Enhancement Suite.
 *
 * @param e mousedown or mousemove event.
 * @return Size for image resizing.
 */
function getDragSize(e)
{
		return (p = Math.pow)(p(e.clientX - (rc = e.target.getBoundingClientRect()).left, 2) + p(e.clientY - rc.top, 2), .5);
}

/*
 * Get the viewport's vertical size. This should work in most browsers. We'll
 * use this when making images fit the screen by height.
 *
 * @return Viewport size.
 */
function getHeight() {
  return window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
}

/*
 * Set up events for the given img element to make it zoomable via
 * drag to zoom. Most of this is taken directly from honestbleeps's
 * Reddit Enhancement Suite. Event functions are currently written
 * inline. For readability, I may move them. But the code is small
 * enough that I don't yet care.
 *
 * @param imgTag Image element.
 */
function makeImageZoomable(imgTag)
{
  DragData = {};

  imgTag.addEventListener('mousedown', function(e)
  {
    if(e.ctrlKey != 0)
      return true;

    /*
     * This is so we can support the command key on Mac. The combination of OS
     * and browser changes how the key is passed to JavaScript. So we're just
     * going to catch all of them. This means we'll also be catching meta keys
     * for other systems. Oh well! Patches are welcome.
     */
    if(e.metaKey != null) // Can be on some platforms
      if(e.metaKey != 0)
        return true;


	if(e.button == 0) {
		DragData.width = e.target.width;
		DragData.delta = getDragSize(e);
		DragData.dragging = true;

		e.preventDefault();
    }

  }, true);

  imgTag.addEventListener('contextmenu', function(e){
	if(imageData[e.target].resized != 0) {
		imageData[e.target].resized = 0;
		e.target.style.zIndex = imageData[e.target].zIndex;
		e.target.style.maxWidth = e.target.style.width = imageData[e.target].width;
		e.target.style.maxHeight = e.target.style.height = imageData[e.target].height;
		e.target.style.position = imageData[e.target].position;

		// Prevent the context menu from actually appearing.
		e.preventDefault();
		e.returnValue = false;
		e.stopPropagation();
		return false;
    }
	return true;

  }, true);
  imgTag.addEventListener('mousemove', function(e)
  {


	if (DragData.dragging){

		clingdelta = Math.abs(DragData.delta - getDragSize(e));

		console.log("Cling [mousemove]: "+clingdelta);

		if (clingdelta > 5) {

			var prevwidth = parseInt(e.target.style.width.replace('px', ''));

			e.target.style.maxWidth = e.target.style.width = Math.floor(((getDragSize(e)) * DragData.width / DragData.delta)) + "px";
			e.target.style.maxHeight = '';
			e.target.style.height = 'auto';
			e.target.style.zIndex = 1000; // Make sure the image is on top.

			if(e.target.style.position == '') {
				e.target.style.position = 'relative';
			}

			imageData[e.target].resized = (prevwidth - parseInt(e.target.style.width.replace('px', '')));
		}
    }
  }, false);

  imgTag.addEventListener('mouseout', function(e) {

	  if (DragData.dragging) {
		DragData.dragging = false;
		e.preventDefault();
		return false;
	  }

	  return true;

  }, true);

  imgTag.addEventListener('mouseup', function(e) {

	  if (DragData.dragging) {
		DragData.dragging = false;
		e.preventDefault();
		return false;
	  }

	  return true;

  }, true);

  imgTag.addEventListener('click', function(e)
  {
    if(e.ctrlKey != 0)
      return true;

    if(e.metaKey != null && e.metaKey != 0) // Can be on some platforms
        return true;

	console.log("Click [click]: "+e.button);
	console.log("Resize [click]: "+imageData[e.target].resized);

    if (!isNaN(imageData[e.target].resized) && imageData[e.target].resized != 0) {
      e.preventDefault();
      return false;
    }

	return true;
  }, true);

}

findAllImages();
document.addEventListener('dragstart', function() {return false}, false);

</script>
</html>

