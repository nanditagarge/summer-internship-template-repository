from PIL import Image
import sys
new_width=int(sys.argv[2])
new_height=int(sys.argv[3])
image = Image.open(r"images/"+sys.argv[1])
ext=sys.argv[1].split('.')[-1]
image = image.resize((new_width, new_height), Image.ANTIALIAS)
quality_val = 100
name=sys.argv[1]
name=name.split('/')[-1]
image.save('images/'+".".join(name.split('.')[:-1])+'.'+ext, quality=quality_val)
print(".".join(name.split('.')[:-1])+'.'+ext)