## Application Code Description

An overview of 
* Code components and their functionality 
    1. Signup Route: Inserting into database
    2. Login Route: Validating from database
    3. Compress route: Huffmann encoding of image and storing in .huff with map in .json
    4. Decompress: Using the json and .huff, image is decompressed and downloaded. 
* Data sources (if any)