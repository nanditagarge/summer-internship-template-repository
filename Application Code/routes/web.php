<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('prelogin');
});
Route::get('/loginpage', function () {
    if(Session::get('id'))
        return redirect('t');
    return view('loginpage');
});
Route::get('/signup', function () {
    if(Session::get('id'))
        return redirect('t');
    return view('signup');
});
Route::get('/logout', function () {
    Auth::logout();
    Session::flush();
    return redirect('loginpage');
});
Route::get('/t', function () {
    if(!Session::get('id'))
        return redirect('loginpage');
    $name=DB::select("select user from dell where id=:id",['id' => Session::get('id')],[1]);
    $name=$name[0]->user;
    $files=DB::select("select url from images where id=:id",['id' => Session::get('id')],[1]);
    // var_dump($files);die;
    return view('postlogin')->with(['image'=>"",'name'=>$name,'files'=>$files]);
});
// Route::get('/postlogin', function () {
//     return view('loginpage');
// });
Route::get('ID/{id}',function($id) {
   echo 'ID: '.$id;
});
Route::post('resize',function() {
	$i=Input::get('img');
   return view('resize')->with(['image'=>$i]);
});

Route::get('delete_img/images/{file}',function($file) {
	$values= array('id' => Session::get('id'),'url' => 'images/'.$file);
	$query = DB::table('images');
	foreach($values as $field => $value) {
    	$query->where($field, $value);
	}
	$query->delete();
	// var_dump($query);die;
   return redirect('t');
});
Route::get('download/images/{file}',function($file) {
   
   return Response::download("decompressed/".pathinfo($file)['filename'].'.png');
});
Route::post('resized',function() {
	$img=Input::get('image');
	$h=Input::get('h');
	$w=Input::get('w');
	// echo "py resiz.py \"{$img}\" {$w} {$h}";die;
   $x=shell_exec("py resiz.py \"{$img}\" {$w} {$h}");
   return redirect("compress/{$x}");
});

Route::get('/lo','App\Http\Controllers\ControllerName@index');
Route::get('/lo/create','App\Http\Controllers\ControllerName@create');
Route::get('/compress/{file}','App\Http\Controllers\ControllerName@store');
Route::get('/crop', function () {
    return view('crop');
});


Route::get('imageform', function()
{
    return View::make('imageform');
});

Route::post('t', function()
{
    $rules = array(
        'image' => 'required|mimes:jpeg,jpg,png,bmp,tiff'
    );
    $files=DB::select("select url from images where id=:id",['id' => Session::get('id')],[1]);
    $validation = Validator::make(Input::all(), $rules);
	$address = Input::get('h');
	if(is_null($address)){
    if ($validation->fails())
    {
    	// echo "die";die;
        var_dump ($validation->messages());
        die;
    }
    else
    {

        $file = Input::file('image');
        $file1="images/".$file;
        
        $file_name = $file->getClientOriginalName();
        $values= array('id' => Session::get('id'),'url' => 'images/'.$file_name);
        DB::table('images')->insert($values);
        if ($file->move('images', $file_name))
        {
            $name=DB::select("select user from dell where id=:id",['id' => Session::get('id')],[1]);
    		$name=$name[0]->user;
    		return view('postlogin')->with(['image'=> $file_name,'name'=>$name,'files'=>$files]);
        	 // return view('postlogin')->with('image',$file_name);
        }
        else
        {
            return "Error uploading file";
        }
    }
}
else
{

	$src  = "images/". Input::get('image');
	$m=explode(".",$src);
	$ext=end($m);
	if(!strcasecmp($ext,"jpg")||!strcasecmp($ext,"jpeg"))
    {$img  = imagecreatefromjpeg($src);

    $dest = ImageCreateTrueColor(Input::get('w'),Input::get('h'));

    imagecopyresampled($dest, $img, 0, 0, Input::get('x'),
        Input::get('y'), Input::get('w'), Input::get('h'),
        Input::get('w'), Input::get('h'));
    imagejpeg($dest, $src);}
    else if(!strcasecmp($ext,"png"))
    {
    	$img  = imagecreatefrompng($src);

    $dest = ImageCreateTrueColor(Input::get('w'),Input::get('h'));

    imagecopyresampled($dest, $img, 0, 0, Input::get('x'),
        Input::get('y'), Input::get('w'), Input::get('h'),
        Input::get('w'), Input::get('h'));
    imagepng($dest, $src);

    }
    else if(!strcasecmp($ext,"bmp"))
    {
    	$img  = imagecreatefrombmp($src);

    $dest = ImageCreateTrueColor(Input::get('w'),Input::get('h'));

    imagecopyresampled($dest, $img, 0, 0, Input::get('x'),
        Input::get('y'), Input::get('w'), Input::get('h'),
        Input::get('w'), Input::get('h'));
    imagebmp($dest, $src);
    }
    else if(!strcasecmp($ext,"tiff"))
    {
    	$img  = imagecreatefromtiff($src);

    $dest = ImageCreateTrueColor(Input::get('w'),Input::get('h'));

    imagecopyresampled($dest, $img, 0, 0, Input::get('x'),
        Input::get('y'), Input::get('w'), Input::get('h'),
        Input::get('w'), Input::get('h'));
    imagetiff($dest, $src);
    }
    else{
    	return "Not Supported Yet";
    }
    $name=DB::select("select user from dell where id=:id",['id' => Session::get('id')],[1]);
    $name=$name[0]->user;
    return view('postlogin')->with(['image'=> Input::get('image'),'name'=>$name,'files'=>$files]);
}
});

Route::get('jcrop', function()
{
    return View::make('jcrop')->with('image', 'images/'. Session::get('image'));
});


Route::get('test', function()
{
   $results = DB::select('select * from dell', [1]);
   return $results;
});
Route::post('test1', 'App\Http\Controllers\ControllerName@login');

Route::post('test', function()
{
    $src  = Input::get('image');
      $file  = imagecreatefromjpeg($src);
      list($width, $height) = getimagesize($file);
    $r = $width / $height;
    if ($crop) {
        if ($width > $height) {
            $width = ceil($width-($width*abs($r-$w/$h)));
        } else {
            $height = ceil($height-($height*abs($r-$w/$h)));
        }
        $newwidth = $w;
        $newheight = $h;
    } else {
        if ($w/$h > $r) {
            $newwidth = $h*$r;
            $newheight = $h;
        } else {
            $newheight = $w/$r;
            $newwidth = $w;
        }
    }
    $src = imagecreatefromjpeg($file);
    $dst = imagecreatetruecolor($newwidth, $newheight);
    imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

    return $dst;
});


Route::post('jcrop', function()
{
    // $quality = 90;

    $src  = Input::get('image');
    $img  = imagecreatefromjpeg($src);
    $dest = ImageCreateTrueColor(Input::get('w'),
        Input::get('h'));

    imagecopyresampled($dest, $img, 0, 0, Input::get('x'),
        Input::get('y'), Input::get('w'), Input::get('h'),
        Input::get('w'), Input::get('h'));
    imagejpeg($dest, $src);

    return View::make('jcrop')->with('image', $src);
});