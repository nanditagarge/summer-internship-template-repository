<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">


        <!--
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
        -->

    </head>
    <body>
        <p>aldhf</p>
        <img src="images/keepthishandy.jpeg" id="main" class="resizeable" alt="Lol"/>

        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.8.2.min.js"><\/script>')</script>
		<?= Form::open() ?>
        <?= Form::hidden('image', 'images/keepthishandy.jpeg') ?>
        <?= Form::hidden('x', '', array('id' => 'x')) ?>
        <?= Form::hidden('y', '', array('id' => 'y')) ?>
        <?= Form::hidden('w', '', array('id' => 'w')) ?>
        <?= Form::hidden('h', '', array('id' => 'h')) ?>
        <?= Form::submit('Crop it!') ?> 
        <?= Form::close() ?>

        <script type="text/javascript">
            function last() {
            	// list($width, $height) = getimagesize($file);
                
            }
        </script>
        <script src="src/images-resizeable.js"></script>
        <script>
            $(document).ready(function() {
                $('.resizeable').each(function(){
                    //'this' inside a jQuery 'each' refers to the 'real' element, which we need to use mousedown/mousemove etc.
                    imagesResizeable.makeImageZoomable(this);
                });
            });
        </script>

        <script type="text/javascript">
        	var imagesResizeable = {

	modules: [],

	init: function() {

		this.modules['showImages'] = {

			"dragTargetData" : {

				"imageWidth" : "",
				"diagonal" : "",
				"dragging" : false

			},

			"getDragSize" : function(e){
				var rc = e.target.getBoundingClientRect();
				var p = Math.pow;
				var dragSize = p(p(e.clientX-rc.left, 2)+p(e.clientY-rc.top, 2), .5);
				var mx = document.getElementById("main");   // Get the element with id="demo"
				var w=mx.clientWidth;
				var h=mx.clientHeight;
                document.getElementById("w").value=w;
                document.getElementById("h").value=h;
				return Math.round(dragSize);
			}

		};
	},

	makeImageZoomable: function(imageTag) {

		var self = this;
		var modules = self.modules;

		// Add listeners for drag to resize functionality...
		imageTag.addEventListener('mousedown', function(e) {
			if (e.button == 0) {
				if (!imageTag.minWidth) imageTag.minWidth = Math.max(1, Math.min(imageTag.width, 100));
				modules['showImages'].dragTargetData.imageWidth = e.target.width;
				modules['showImages'].dragTargetData.diagonal = modules['showImages'].getDragSize(e);
				modules['showImages'].dragTargetData.dragging = false;
				e.preventDefault();
				var mx = document.getElementById("main");   // Get the element with id="demo"
				var w=mx.clientWidth;
				var h=mx.clientHeight;
                document.getElementById("w").value=w;
                document.getElementById("h").value=h;
			}
		}, true);

		imageTag.addEventListener('mousemove', function(e) {
			if (modules['showImages'].dragTargetData.diagonal){
				var newDiagonal = modules['showImages'].getDragSize(e);
				var oldDiagonal = modules['showImages'].dragTargetData.diagonal;
				var imageWidth = modules['showImages'].dragTargetData.imageWidth;
				e.target.style.maxWidth=e.target.style.width=Math.max(e.target.minWidth, newDiagonal/oldDiagonal*imageWidth)+'px';

				e.target.style.maxHeight='';
				e.target.style.height='auto';
				modules['showImages'].dragTargetData.dragging = true;
				var mx = document.getElementById("main");   // Get the element with id="demo"
				var w=mx.clientWidth;
				var h=mx.clientHeight;
                document.getElementById("w").value=w;
                document.getElementById("h").value=h;
			}
		}, false);

		imageTag.addEventListener('mouseout', function(e) {
			modules['showImages'].dragTargetData.diagonal = 0;
		}, false);

		imageTag.addEventListener('mouseup', function(e) {
			if (modules['showImages'].dragTargetData.diagonal) {
				var newDiagonal = modules['showImages'].getDragSize(e);
				var oldDiagonal = modules['showImages'].dragTargetData.diagonal;
				var imageWidth = modules['showImages'].dragTargetData.imageWidth;
				e.target.style.maxWidth=e.target.style.width=Math.max(e.target.minWidth, newDiagonal/oldDiagonal*imageWidth)+'px';
			}

			modules['showImages'].dragTargetData.diagonal = 0;
			var mx = document.getElementById("main");   // Get the element with id="demo"
				var w=mx.clientWidth;
				var h=mx.clientHeight;
                document.getElementById("w").value=w;
                document.getElementById("h").value=h;
		}, false);

		imageTag.addEventListener('click', function(e) {
			modules['showImages'].dragTargetData.diagonal = 0;
			var mx = document.getElementById("main");   // Get the element with id="demo"
				var w=mx.clientWidth;
				var h=mx.clientHeight;
                document.getElementById("w").value=w;
                document.getElementById("h").value=h;
			if (modules['showImages'].dragTargetData.dragging) {
				modules['showImages'].dragTargetData.dragging = false;
				e.preventDefault();
				return false;
			}
		}, false);

	}

}

imagesResizeable.init();
        </script>

    </body>
</html>