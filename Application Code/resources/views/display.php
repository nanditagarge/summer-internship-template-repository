<!DOCTYPE html>
<html>
<head>
	<title>fial</title>
	<meta charset="utf-8">
        <link rel="stylesheet" href="css/jquery.Jcrop.min.css" />
				<link rel="stylesheet" href="css/outputinfo.css">
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script src="js/jquery.Jcrop.min.js"></script>
				<style>
				     @import url(https://fonts.googleapis.com/css?family=Raleway);
             body{
             	font-family: arial;
							background-color: #EFEFEF;
							color:#454545;
						}
						a{
							color:#007DB8;
						}
						a:hover{
							color:#0A5665;
						}
						.row{
							display: flex;
						}
						.column{
							flex: 50%;
							padding:10px;
						}
				</style>
</head>
<body style="text-align:center;">
	<div class="row">
		<div class="column">
			<h2>The Image</h2>
		<img src ="<?=url('/').'/images/'.$ofile?>" style="max-width: 60%;height: auto">
		</div>
	</div>
	<br>
	<br>
<h3>Original Bits </h3>
<p class="output"><?=$osize?> bits </p>
<h3>Compressed Bits </h3>
<p class="output"><?=$csize?> bits </p>
<h3>Compression Percentage </h3>
<p class="output"><?=$saved?> %</p>
<br>
<br>
<br>
<a  href="../t"><h3>Compress Another Image</h3></a>

</body>
</html>
