<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge"> 
		<meta name="viewport" content="width=device-width, initial-scale=1"> 
		<title>Image Resizing</title>
		<meta name="description" content="Learn how to resize images using JavaScript and the HTML5 Canvas element using controls, commonly seen in photo editing applications." />
		<meta name="keywords" content="canvas, javascript, HTML5, resizing, images" />
		<meta name="author" content="Codrops" />
		<link rel="shortcut icon" href="../favicon.ico">
		<link rel="stylesheet" type="text/css" href="css/normalize.css" />
		<link rel="stylesheet" type="text/css" href="css/demo.css" />
		<link rel="stylesheet" type="text/css" href="css/component.css" />
	</head>
	<body onload="some()">
		<div class="container">
			<div class="content">
				<header class="codrops-header">
					<h1>Image Resizing</h1>
				</header>
				<div class="component">
					<div class="overlay">
						<div class="overlay-inner">
						</div>
					</div>
					<!-- This image must be on the same domain as the demo or it will not work on a local file system -->
					<!-- http://en.wikipedia.org/wiki/Cross-origin_resource_sharing -->
					<img id="resize" class="resize-image" src="images/<?=$image?>" alt="image for resizing">
				</div>
				
				<div>
					Height<input type="number" id="h" readonly>
					Width<input type="number" id="w" readonly>
				</div>
				<div class="a-tip">
					<p><strong>Tip:</strong> hold <span>SHIFT</span> while resizing to keep the original aspect ratio of the image.</p><?= Form::open(array('url' => 'resized', 'files'=>true,'method'=>'post') ) ?>
		        <?= Form::hidden('image', $image) ?>
		        <?= Form::hidden('w', '', array('id' => 'wf')) ?>
		        <?= Form::hidden('h', '', array('id' => 'hf')) ?>
		        <?= Form::submit('Save') ?> 
		        <?= Form::close() ?>
				</div>
				
			</div>	
		</div> 
		<script src="js/jquery-2.1.1.min.js"></script>
		<script src="js/component.js"></script>
		<script type="text/javascript">
			function some() {
				var mx=document.getElementById('resize');
				var w=mx.clientWidth;
				var h=mx.clientHeight;
				document.getElementById('h').value=h;
				document.getElementById('w').value=w;
			}
			function save() {
				var mx=document.getElementById('resize');
				var w=mx.clientWidth;
				var h=mx.clientHeight;
				document.getElementById('hf').value=h;
				document.getElementById('wf').value=w;
			}
		</script>
		<script>
			$(document).ready(function(){
					 $(".component").on("click change mouseenter mousedown mouseleave mouseover mouseout mousein", function() {
					 	$("#h").val($("#resize").height());
					 	$("#w").val($("#resize").width());
					 	$("#hf").val($("#resize").height());
					 	$("#wf").val($("#resize").width());
					 });
			});
		</script>
	</body>
</html>